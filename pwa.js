// const staticCacheName = 's-app-v7'
// const dynamicCacheName = 'd-app-v3'

// console.log("SW init")

// const assetUrls = [ 
//     'css/style.css',
//     'index.html',
//     'https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js',
//     'js/startSW.js',
//     'js/script.js',
//     'image/default-avatarr.jpg',
//     'image/backgroundPage.jpg',
//     'manifest.json'
// ]

// self.addEventListener('install', async event => {
//   const cache = await caches.open(staticCacheName)
//   try {    
//     await cache.addAll(assetUrls)
//   } catch (error) {
//     console.log("error cache.addAll ",error)
//   }
// })

// self.addEventListener('activate', async event => {
//   console.log("SW activate")
//   const cacheNames = await caches.keys()
//   await Promise.all(
//     cacheNames
//       .filter(name => name !== staticCacheName)
//       .filter(name => name !== dynamicCacheName)
//       .map(name => caches.delete(name))
//   )
// })

// self.addEventListener('fetch', event => {
//   const {request} = event

//   const url = new URL(request.url)
//   if (url.origin === location.origin) {
//     event.respondWith(cacheFirst(request))
//   } else {
//     console.log("SW networkFirst")
    
//   }
// })


// async function cacheFirst(request) {
//   const cached = await caches.match(request)
//   return cached ?? await fetch(request)
// }

