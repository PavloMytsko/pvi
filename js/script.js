
let tableArray = [
    
];

function reloadTable() {
  
let table = document.getElementById("idStudentsTable");
table.innerHTML = ''
tableArray.forEach((element) => {
let row = 
`
<tr>
    <td><input type="checkbox"></td>
    <td> ${element.group} </td>
    <td>${element.firstname}</td>
    <td>${element.lastname}</td>
    <td>${element.gender}</td>
    <td>${element.birthday}</td>
    <td> <div class="user-status"></div> </td>
    
        <td>
        <span class="button-edit" onclick="editStudent(${element.id})" >
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 50 50">
        <path d="M 43.125 2 C 41.878906 2 40.636719 2.488281 39.6875 3.4375 L 38.875 4.25 L 45.75 11.125 C 45.746094 11.128906 46.5625 10.3125 46.5625 10.3125 C 48.464844 8.410156 48.460938 5.335938 46.5625 3.4375 C 45.609375 2.488281 44.371094 2 43.125 2 Z M 37.34375 6.03125 C 37.117188 6.0625 36.90625 6.175781 36.75 6.34375 L 4.3125 38.8125 C 4.183594 38.929688 4.085938 39.082031 4.03125 39.25 L 2.03125 46.75 C 1.941406 47.09375 2.042969 47.457031 2.292969 47.707031 C 2.542969 47.957031 2.90625 48.058594 3.25 47.96875 L 10.75 45.96875 C 10.917969 45.914063 11.070313 45.816406 11.1875 45.6875 L 43.65625 13.25 C 44.054688 12.863281 44.058594 12.226563 43.671875 11.828125 C 43.285156 11.429688 42.648438 11.425781 42.25 11.8125 L 9.96875 44.09375 L 5.90625 40.03125 L 38.1875 7.75 C 38.488281 7.460938 38.578125 7.011719 38.410156 6.628906 C 38.242188 6.246094 37.855469 6.007813 37.4375 6.03125 C 37.40625 6.03125 37.375 6.03125 37.34375 6.03125 Z"></path>
        </svg>
        </span>
        <span class="delete-Students-Button" onclick="delete_Students(${element.id})">
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 48 48">
        <path d="M 20.5 4 A 1.50015 1.50015 0 0 0 19.066406 6 L 14.640625 6 C 12.803372 6 11.082924 6.9194511 10.064453 8.4492188 L 7.6972656 12 L 7.5 12 A 1.50015 1.50015 0 1 0 7.5 15 L 8.2636719 15 A 1.50015 1.50015 0 0 0 8.6523438 15.007812 L 11.125 38.085938 C 11.423352 40.868277 13.795836 43 16.59375 43 L 31.404297 43 C 34.202211 43 36.574695 40.868277 36.873047 38.085938 L 39.347656 15.007812 A 1.50015 1.50015 0 0 0 39.728516 15 L 40.5 15 A 1.50015 1.50015 0 1 0 40.5 12 L 40.302734 12 L 37.935547 8.4492188 C 36.916254 6.9202798 35.196001 6 33.359375 6 L 28.933594 6 A 1.50015 1.50015 0 0 0 27.5 4 L 20.5 4 z M 14.640625 9 L 33.359375 9 C 34.196749 9 34.974746 9.4162203 35.439453 10.113281 L 36.697266 12 L 11.302734 12 L 12.560547 10.113281 A 1.50015 1.50015 0 0 0 12.5625 10.111328 C 13.025982 9.4151428 13.801878 9 14.640625 9 z M 11.669922 15 L 36.330078 15 L 33.890625 37.765625 C 33.752977 39.049286 32.694383 40 31.404297 40 L 16.59375 40 C 15.303664 40 14.247023 39.049286 14.109375 37.765625 L 11.669922 15 z"></path>
        </svg>
        </span>
        </td>
   
</tr>
`
table.innerHTML +=row;
});

}

// function checkValidity() {
//   let inputValueFirstName = document.getElementById("firstname").value;
//   let inputValueLastName = document.getElementById("lastname").value;

//   let check = /^[A-Z][a-z\s]*[A-Z]?[a-z\s]*$/;

//   if (!check.test(inputValueFirstName)) {
//       alert("Введені дані для імені некоректні!");
//   }

//   if (!check.test(inputValueLastName)) {
//       alert("Введені дані для прізвища некоректні!");
//   }
// }

function get_student() {
  $.get("http://127.0.0.1:3000/api/students", function (data, status) {
      // alert("Data: " + data[0]["firstname"] + "\nStatus: " + status);
      for (var i = 0; i < data.length; i++) {
        data[i]["birthday"] = data[i]["birthday"].split("T")[0];
    }
    
      tableArray = data;
      reloadTable();
  });
}

get_student();

let modeDisplay = false;

let buttonAdd = document.getElementById('add-Students-Button')

buttonAdd.addEventListener("click", function() {

  AddEditStudentsclick.style.display = 'block';

  document.getElementById('group').value = "";
  document.getElementById('firstname').value = "";
  document.getElementById('lastname').value = "";
  document.getElementById('gender').value = "";
  document.getElementById('birthday').value = "";
  document.getElementById('id').value = "";
 

  setFormMode(false);

});

reloadTable ();

function delete_el(id_el) {
  let index = -1;
  for (let i = 0; i < tableArray.length; i++) {
      if (tableArray[i].id == id_el) {
          index = i;
          break;
      }
  }
  if (index >= 0) {
      tableArray.splice(index, 1);
      reloadTable();
  }
}

try {
  
function delete_Students(id_el) {
  $.ajax({
      type: "DELETE",
      url: "http://127.0.0.1:3000/api/students/" + id_el,
      success: function (data) {
          if (data == '0') {
              alert("Помилка видалення");
              return;
          }
          delete_el(id_el);
      },
      error: function () {
          alert("Помилка видалення");
      }
  });
}
} catch (error) {
  alert("Ooops")
}

let buttonProfile = document.getElementById('user-profile-click')
let buttonForm = document.getElementById('userProfileAfterClick_back_')
let mainForm = document.getElementById('userProfileAfterClick_')
buttonProfile.addEventListener("click", function() {
 
  if (mainForm.style.display === 'block') {
    mainForm.style.display = 'none'; 
  } else {
    mainForm.style.display = 'block'; 
  }
});
 
let bellButton = document.getElementById('bell-click')
let messageProfile = document.getElementById('Message-Students-click')
bellButton.addEventListener("click", function() {
 
  if (messageProfile.style.display === 'block') {
    messageProfile.style.display = 'none'; 
  } else {
    messageProfile.style.display = 'block'; 
  }
});


let AddEditStudentsclick = document.getElementById('Add-Edit-Students-click')

function editStudent(id){
  AddEditStudentsclick.style.display = 'block'; 

  let index = -1;
  for (let i = 0; i < tableArray.length; i++) {
      if (tableArray[i].id == id) {
          index = i;
          break;
      }
  }
  if (index == -1) {
     return;
  }
  AddEditStudentsclick.style.display='block';
  const el= tableArray[index];

  document.getElementById('group').value = el.group;
  document.getElementById('firstname').value = el.firstname;
  document.getElementById('lastname').value = el.lastname;
  document.getElementById('gender').value = el.gender;
  document.getElementById('birthday').value = el.birthday;
  document.getElementById('id').value = el.id;

  setFormMode(true);
}

let exitButton = document.getElementById('AddEditStudentsExitButton')
exitButton.addEventListener("click", function() {
 
  AddEditStudentsclick.style.display = 'none'; 

});

// Функція для оновлення інформації про студента в tableArray
function updateStudentInTableArray(updatedStudent) {
  const index = tableArray.findIndex(student => student.id === updatedStudent.id);
  if (index !== -1) {
      tableArray[index] = updatedStudent;
  }
}
// let okButton = document.getElementById('buttonStudentsOk')
// okButton.addEventListener("click", function() {
 
//   AddEditStudentsclick.style.display = 'none'; 

// });

function setFormMode(displayMode) {
  modeDisplay = displayMode;

  if (modeDisplay) {
      document.getElementById('buttonStudentsCreate').style.display = "none";
      document.getElementById('buttonStudentsSave').style.display = "block";

      document.getElementById('addTitle').style.display = "none";
      document.getElementById('editTitle').style.display = "block";
  } else {
      document.getElementById('buttonStudentsCreate').style.display = "block";
      document.getElementById('buttonStudentsSave').style.display = "none";

      document.getElementById('addTitle').style.display = "block";
      document.getElementById('editTitle').style.display = "none";
  }
}
 
try {
  //добавлення та редагування
  $(document).ready(function () {
      $(document).on('submit', '#formElem', function () {
          if (modeDisplay) {
              $('#id').prop('disabled', false);
          }
          else {
              $('#id').prop('disabled', true);
          }
          let dataString = $('#formElem').serialize();
          console.log(dataString);
          let url = "http://127.0.0.1:3000/api/students";
          if (modeDisplay) {
              url += "/" + $('#id').val(); 
          }
          $.ajax({
              type: modeDisplay ? "PUT" : "POST",
              url: url,
              data: dataString,
              success: function (data) {
                  if (modeDisplay) {
                      // delete_el(data.id);
                      updateStudentInTableArray(data);
                      location.reload();
                  } else {
                      tableArray.push(data);
                  }
                  reloadTable();
              },
              error: function (data) {
                  console.log(data);
                  if (modeDisplay) {
                      alert("Помилка редагування: " + data.responseText)
                  } else {
                      alert("Помилка додавання: " + data.responseText)
                  }
              }
          });
          return false;
      });
  });
} catch (error) {
  console.error(error);
}

function closeFormSignIn(){
  let AddEditStudentsclick = document.getElementById('SignInBlock');
  AddEditStudentsclick.style.display = 'none';
}

$(document).on('submit', '#SignInForm', function () {
  closeFormSignIn();
  document.getElementById('user-profile-click').style.display = "flex";
  document.getElementById('openSignInForm').style.display = "none";
  document.getElementById('bell-click').style.display = "flex";
  return false;
});

// хрестик форми входу
document.getElementById("SignInExitButton").onclick = function () {
  closeFormSignIn();
}

let message = document.getElementById('Message-Students-click')
let messageTag = document.getElementById('message-tag')
messageTag.addEventListener("click", function() {

  message.style.display = 'block';
}); 

let ExtiButtonMessage = document.getElementById('ExitMessageButtonClick')
ExtiButtonMessage.addEventListener("click", function() {

  message.style.display = 'none';
}); 

const msgerForm = get(".msger-inputarea");
const msgerInput = get(".msger-input");
const msgerChat = get(".msger-chat");

const BOT_MSGS = [
  "Hi, how are you?",
  "Ohh... I can't understand what you trying to say. Sorry!",
  "I like to play games... But I don't know how to play!",
  "Sorry if my answers are not relevant. :))",
  "I feel sleepy! :("
];

// Icons made by Freepik from www.flaticon.com
const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
const BOT_NAME = "BOT";
const PERSON_NAME = "Sajad";

msgerForm.addEventListener("submit", event => {
  event.preventDefault();
// const groupId = chatRoomContainer.getAttribute('data-group-id');
  const msgText = msgerInput.value;
  if (!msgText) return;

  appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);
  msgerInput.value = "";

  botResponse();
});

function appendMessage(name, img, side, text) {
  //   Simple solution for small apps
  const msgHTML = `
    <div class="msg ${side}-msg">
      <div class="msg-img" style="background-image: url(${img})"></div>

      <div class="msg-bubble">
        <div class="msg-info">
          <div class="msg-info-name">${name}</div>
          <div class="msg-info-time">${formatDate(new Date())}</div>
        </div>

        <div class="msg-text">${text}</div>
      </div>
    </div>
  `;

  msgerChat.insertAdjacentHTML("beforeend", msgHTML);
  msgerChat.scrollTop += 500;
}

function botResponse() {
  const r = random(0, BOT_MSGS.length - 1);
  const msgText = BOT_MSGS[r];
  const delay = msgText.split(" ").length * 100;

  setTimeout(() => {
    appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
  }, delay);
}

// Utils
function get(selector, root = document) {
  return root.querySelector(selector);
}

function formatDate(date) {
  const h = "0" + date.getHours();
  const m = "0" + date.getMinutes();

  return `${h.slice(-2)}:${m.slice(-2)}`;
}

function random(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

$('#add-chatroom').click(function(){

})

//logOut
$('#logOut').click(function(){
  // document.getElementById('userProfileAfterClick_').style.display = "block";
  document.getElementById('user-profile-click').style.display = "none";
  document.getElementById('openSignInForm').style.display = "block";
  document.getElementById('userProfileAfterClick_').style.display = "none";
  document.getElementById('bell-click').style.display = "none";
  
})

//open signInForm
$('#openSignInForm').click(function(){
  document.getElementById('SignInBlock').style.display = "block";

})
const mainChatWindow = document.querySelector('.main-chat-window');
document.addEventListener('DOMContentLoaded', function() {
  const chatContainer = document.getElementById('chat-container');
  const chats = [];

  const chatGroups = [
    { id: 1, users: ['User1'] },
    { id: 2, users: ['User2'] },
    { id: 3, users: ['User3'] }
  ];

  chatGroups.forEach(function(group) {
    const chatRoomContainer = createChatRoomContainer(group);
    chatContainer.appendChild(chatRoomContainer);
  });

  
  let groupIdCounter = 4;
  const addButton = document.getElementById('add-chatroom');
  addButton.addEventListener('click', function() {
    const newGroupId = generateGroupId();
    const newGroup = { id: newGroupId, users: ['User' + newGroupId] };
    chatGroups.push(newGroup);
    const chatRoomContainer = createChatRoomContainer(newGroup);
    chatContainer.appendChild(chatRoomContainer);
    chatRoomContainer.setAttribute('data-group-id', newGroupId); 
  });
  

  function createChatRoomContainer(group) {
    const chatRoomContainer = document.createElement('div');
    chatRoomContainer.classList.add('Scroll-Group');
    chatRoomContainer.setAttribute('data-group-id', group.id); 

    group.users.forEach(function(user) {
      const userProfile = createProfileElement(user);
      chatRoomContainer.appendChild(userProfile);
    });

    const chatWindow = document.createElement('div');
    chatWindow.classList.add('chat-window');
    chatRoomContainer.appendChild(chatWindow);

    chatRoomContainer.addEventListener('click', function() {
      const groupId = chatRoomContainer.getAttribute('data-group-id'); 
      console.log('Clicked on group with ID:', groupId);
      const chat = chats.find(chat => chat.groupId === groupId);
      if (chat) {
        displayChat(chat, chatWindow);
      } else {
        const newChat = createNewChat(groupId);
        chats.push(newChat);
        displayChat(newChat, chatWindow);
      }
    });
    
    return chatRoomContainer;
  }

  function createNewChat(groupId) {
    const newChat = { groupId, messages: [] };
    return newChat;
  }

  function displayChat(chat, chatWindow) {
    chatWindow.innerHTML = '';
    chat.messages.forEach(function(message) {
      const messageElement = createMessageElement(message.sender, message.text);
      chatWindow.appendChild(messageElement);
    });
  }

  function createProfileElement(role) {
    const profileContainer = document.createElement('div');
    profileContainer.classList.add('chatroom-textbox-profile');
  
    const chatRoomProfile = document.createElement('div');
    chatRoomProfile.classList.add('ChatRoomProfile');
  
    const avatar = document.createElement('img');
    avatar.src = 'image/default-avatarr.jpg';
    avatar.classList.add('ChatRoomProfileAvatar');
  
    const nameProfile = document.createElement('div');
    nameProfile.classList.add('ChatRoom-NameProfileAdmin');
    nameProfile.textContent = role;
  
    chatRoomProfile.appendChild(avatar);
    chatRoomProfile.appendChild(nameProfile);
    profileContainer.appendChild(chatRoomProfile);
  
    return profileContainer;
  }
  
  function generateGroupId() {
    return groupIdCounter++;
  }

  const msgerForm = document.querySelector(".msger-inputarea");
  const msgerInput = document.querySelector(".msger-input");

  msgerForm.addEventListener("submit", function(event) {
    event.preventDefault();
    const msgText = msgerInput.value;
    if (!msgText) return;

    const openChatWindow = document.querySelector('.chat-window');
    if (!openChatWindow) {
      alert('Please select a chat room first');
      return;
    }

    const groupId = openChatWindow.parentElement.dataset.groupId;
    const chat = chats.find(chat => chat.groupId === groupId);

    if (!chat) {
      alert('Error: Chat not found');
      return;
    }

    const message = { sender: 'You', text: msgText };
    chat.messages.push(message);

    const messageElement = createMessageElement('You', msgText);
    openChatWindow.appendChild(messageElement);

    msgerInput.value = "";
    botResponse(openChatWindow);
  });

  function createMessageElement(sender, text) {
    const msgContainer = document.createElement('div');
    msgContainer.classList.add('msg', sender === 'You' ? 'right-msg' : 'left-msg');
  
    const msgImg = document.createElement('div');
    msgImg.classList.add('msg-img');
    msgImg.style.backgroundImage = `url(${sender === 'You' ? PERSON_IMG : BOT_IMG})`;
  
    const msgBubble = document.createElement('div');
    msgBubble.classList.add('msg-bubble');
  
    const msgInfo = document.createElement('div');
    msgInfo.classList.add('msg-info');
  
    const msgName = document.createElement('div');
    msgName.classList.add('msg-info-name');
    msgName.textContent = sender;
  
    const msgTime = document.createElement('div');
    msgTime.classList.add('msg-info-time');
    msgTime.textContent = formatDate(new Date());
  
    const msgText = document.createElement('div');
    msgText.classList.add('msg-text');
    msgText.textContent = text;
  
    msgInfo.appendChild(msgName);
    msgInfo.appendChild(msgTime);
    msgBubble.appendChild(msgInfo);
    msgBubble.appendChild(msgText);
    msgContainer.appendChild(msgImg);
    msgContainer.appendChild(msgBubble);
  
    return msgContainer;
  }

  function botResponse(chatWindow) {
    const r = random(0, BOT_MSGS.length - 1);
    const msgText = BOT_MSGS[r];
    const delay = msgText.split(" ").length * 100;
    setTimeout(() => {
      const messageElement = createMessageElement(BOT_NAME, msgText);
      chatWindow.appendChild(messageElement);
    }, delay);
  }
});
